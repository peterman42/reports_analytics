/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief The source file provides the functions declarations which provide the report analytics
 *
 * <Add Extended Description Here>
 *
 * @author Petros Morakos
 * @date 2020-12-14
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
    print_statistics(test, SIZE);
}


void print_statistics(unsigned char* data, unsigned int length)
{
    printf("The mean value of array is :: %.5f\n", find_mean(data, length));
    printf("The median value of array is :: %.5f\n", find_median(data, length));
    printf("The maximum value of array is :: %d\n", find_maximum(data, length));
    printf("The minimum value of array is :: %d\n", find_minimum(data, length));

    sort_array(data, length);
    printf("Sorted array: \n");
    print_array(data, length);
}

void print_array(unsigned char* data, unsigned int length)
{
    if(data == NULL) return;

    for(unsigned int i=0; i < length;i++){
        printf("%d\n",data[i]);
    }
}

double find_median(unsigned char* data, unsigned int length)
{
    if(data == NULL) return -1;
    if(length == 0) return -1;

    unsigned int median = 0;
    unsigned result = length % 2;
    if(result == 0)
    {
        unsigned int middle = length/2;
        median = (double)((data[middle] + data[middle-1]) / 2);
    }
    else
    {
        median = data[result+1];
    }

    return median;
}

double find_mean(unsigned char* data, unsigned int length)
{
    if(data == NULL) return -1;
    if(length == 0) return -1;

    unsigned int mean;
    unsigned int sum = 0;
    for(unsigned int i=0; i < length;i++){
        sum += data[i];
    }

    return (double)sum/length;
}

int find_maximum(unsigned char* data, unsigned int length)
{
    if(data == NULL) return -1;

    unsigned int max = 0;
    for(unsigned int i =0; i< length; i++)
    {
        if(data[i] > max) max = data[i];
    }

    return max;
}

int find_minimum(unsigned char* data, unsigned int length)
{
    if(data == NULL) return -1;

    unsigned int min = 255;
    for(unsigned int i =0; i< length; i++)
    {
        if(data[i] < min) min = data[i];
    }

    return min;
}

void sort_array(unsigned char* data, int length)
{
    if(data == NULL) return;

    unsigned int temp = 0;
    for(unsigned int i =0; i<length;i++)
    {
        for(unsigned int j = i+1; j <length; j++)
        {
            if(data[i] < data[j])
            {
                temp = data[i];
                data[i] = data[j];
                data[j] = temp;
            }
        }
    }
}
