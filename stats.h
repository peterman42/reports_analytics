/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief The header file provides the functions declarations which provide the report analytics
 *
 * The file contains the declaration of the following functions:
 *      * print_statistics
 *      * print_array
 *      * find_median
 *      * find_mean
 *      * find_maximum
 *      * find_minimum
 *      * sort_array
 *
 * @author Petros Morakos
 * @date 2020-12-14
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/**
 * @brief Prints the data statistics
 *
 * The function prints the statistics of the given array. In particular,
 * it prints the minimmum item, the maximmum item, the mean and the median.
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return void
 */
void print_statistics(unsigned char* data, unsigned int length);

/**
 * @brief Prints data array
 *
 * The function prints the array items.
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return void
 */
void print_array(unsigned char* data, unsigned int length);

/**
 * @brief Finds the median in an array
 *
 * The function given an array of data and a length, returns the median value
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return the median in the array
 */
double find_median(unsigned char* data, unsigned int length);

/**
 * @brief Finds the mean
 *
 * The function given an array of data and a length, returns the mean
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return the mean value
 */
double find_mean(unsigned char* data, unsigned int length);

/**
 * @brief Finds the maximum value
 *
 * The function given an array of data and a length, returns the maximum
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return the maximum value in the array
 */
int find_maximum(unsigned char* data, unsigned int length);

/**
 * @brief Finds the minimum value
 *
 * The function given an array of data and a length, returns the minimum
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return the minimum value in the array
 */
int find_minimum(unsigned char* data, unsigned int length);

/**
 * @brief Sorts an array
 *
 * The function given an array of data and a length, sorts the array from largest to smallest in descending order.
 *
 * @param data The pointer which points to the data array
 * @param length The length of the data array
 *
 * @return void
 */
void sort_array(unsigned char* data, int length);

#endif /* __STATS_H__ */
